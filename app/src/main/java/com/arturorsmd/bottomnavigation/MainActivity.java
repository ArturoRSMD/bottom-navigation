package com.arturorsmd.bottomnavigation;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.arturorsmd.bottomnavigation.Fragments.BackupFragment;
import com.arturorsmd.bottomnavigation.Fragments.HomeFragment;
import com.arturorsmd.bottomnavigation.Fragments.NotificationsFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(itemSelectedListener);

        showFragment(new HomeFragment());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener itemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()){
                        case R.id.nav_item_home:
                            showFragment(new HomeFragment());
                            break;
                        case R.id.nav_item_backup:
                            showFragment(new BackupFragment());
                            break;
                        case R.id.nav_item_notifications:
                            showFragment(new NotificationsFragment());
                            break;
                    }

                    return true;
                }
            };

    public void showFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

}
