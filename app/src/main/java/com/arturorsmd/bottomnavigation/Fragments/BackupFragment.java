package com.arturorsmd.bottomnavigation.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arturorsmd.bottomnavigation.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class BackupFragment extends Fragment {


    public BackupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_backup, container, false);
        return view;
    }

}
